# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SPHINXPROJ    = UsabilityBestPractices
SOURCEDIR     = .
BUILDDIR      = _build
TEX_MASTER    = main.tex
BIB           = main.bib
CLS           = IEEEtran.cls
ALL_DEP       = ${BIB} ${CLS}
ALL           = ${TEX_MASTER} ${ALL_DEP}
POSTFIXES     = aux log pdf lot out pdfsync toc blg lof bbl

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help clean Makefile ${ALL_DEP}

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile main.pdf slides.pdf
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

main.pdf: main.tex ${ALL_DEP}
	latexmk -pdf -halt-on-error $<

slides.pdf: slides.tex ${ALL_DEP}
	latexmk -pdf -halt-on-error $<

clean:
	$(foreach name,$(ALL),$(RM) $(addprefix $(basename $(name)).,$(POSTFIXES));)
	$(RM) com.aux
