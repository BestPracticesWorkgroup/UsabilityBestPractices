Report on: Software Best Practices for Undergraduates
=====================================================

# Participants:
* Jonah Miller
* Aleksandra Nenadic
* Raniere Silva
* Francisco Queiroz
* Hans Fangohr
* Prahbjyot Sing

# Motivation:

We were motivated by the perceived prevalence of "hidden code" in
  scientific communities: code written by researchers in an
  unsustainable way that is never shared with the larger community.

# Original objective:

To implement a course describing software best-practices aimed at
domain science.

# What we actually did:

We were unable to muster the time or momentum to facilitate our
original goal. However, thanks to the leadership and efforts of
Francisco Queiroz, we were able to come together to write a paper
discussing best practices for user interface design and to provide
some recomendations.

Our recomendations help address hidden code, if they are ever
read by authors of hidden code. However, a course curriculum would
still be valuable.

# Next steps

Return to the original SMART steps we mapped out.