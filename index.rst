.. Usability Best Practices documentation master file, created by
   sphinx-quickstart on Tue Jul  4 09:54:36 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Abstract
========

Full Text
---------

:download:`Usability Best Practices is available as PDF <main.pdf>`.

Slides
------

:download:`Slides presented at WSSSPE 5.1 are available as PDF <slides.pdf>`.

How to cite
-----------
